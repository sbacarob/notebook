class Notebook < ActiveRecord::Base
	belongs_to :user
	has_many :topics, dependent: :destroy
	has_many :entries
end
