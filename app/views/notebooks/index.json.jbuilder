json.array!(@notebooks) do |notebook|
  json.extract! notebook, :id, :user_id, :name, :subject, :created, :cover
  json.url notebook_url(notebook, format: :json)
end
