class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :topic_id
      t.text :name
      t.text :content
      t.date :created

      t.timestamps
    end
  end
end
