class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.integer :notebook_id
      t.text :name

      t.timestamps
    end
  end
end
